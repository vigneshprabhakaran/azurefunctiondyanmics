using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Xrm.Tooling.Connector;
using static SegmentCallPlan.Common;

namespace SegmentCallPlan.Functions
{
    public static class CallPlanDistributionOperation
    {
        #region Private Properties
        private static CrmServiceClient _Service { get; set; }
        private static TraceWriter _Logger { get; set; }
        #endregion

        [FunctionName("CallPlanDistributionOperation")]
        public static void Run([QueueTrigger("segment-call-plan-ids")]string segmentCallPlanId, TraceWriter log)
        {
            try
            {
                log.Info($"Create Sales Call Plan for : {segmentCallPlanId}");

                _Logger = log;

                _Service = GetCrmServiceClient(log);

                Guid segmentCallPlanIdGuid = new Guid(segmentCallPlanId);

                CallPlanDistributionHelper.Distribute(segmentCallPlanIdGuid, _Service, _Logger);
            }
            catch (Exception ex)
            {
                log.Error($"Error - Message: {ex.Message} ");
            }

        }
    }
}
