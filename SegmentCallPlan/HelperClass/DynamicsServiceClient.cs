﻿using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegmentCallPlan
{
    public class DynamicsServiceClient
    {
        private static DynamicsServiceClient instance = null;
        private static object syncLock = new object();
        public static DynamicsServiceClient Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new DynamicsServiceClient();
                        }
                    }
                }
                return instance;
            }
        }

        List<CrmServiceClient> serviceClientsList = new List<CrmServiceClient>();

        public DynamicsServiceClient()
        {
            for (int i = 1; i <= 6; i++)
            {
                string serviceAccount = "IndegeneServiceAccount" + i;
                CrmServiceClient tempSvcClient = new CrmServiceClient(ConfigurationManager.ConnectionStrings[serviceAccount].ConnectionString);
                serviceClientsList.Add(tempSvcClient);
            }
        }

        private Random _random = new Random();

        public CrmServiceClient GetCrmServiceClientMultiUser(Microsoft.Azure.WebJobs.Host.TraceWriter _Logger)
        {
            int r = _random.Next(serviceClientsList.Count);
            string serviceAccount = "IndegeneServiceAccount" + r;
            _Logger.Info($"\tConnecting to {serviceAccount}");
            return serviceClientsList[r];
        }
    }
}
