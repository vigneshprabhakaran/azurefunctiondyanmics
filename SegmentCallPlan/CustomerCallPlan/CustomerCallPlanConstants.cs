﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SegmentCallPlan
{
    public static class CustomerCallPlanConstants
    {
        public static string GetCustomerSegmentationFetchXML(EntityReference product, EntityReference segment)
        {
            return @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                      <entity name='indskr_productrating'>
                        <attribute name='indskr_contact' />
                        <attribute name='indskr_productratingid' />
                        <filter type='and'>
                           <condition attribute='indskr_product' operator='eq' uitype='product' value='" + product.Id + @"' />
                           <condition attribute='indskr_segmentation' operator='eq' uitype='indskr_customersegmentation' value='" + segment.Id + @"' />
                        </filter>
                        <link-entity name='contact' from='contactid' to='indskr_contact' link-type='inner' alias='aa'>
                          <filter type='and'>
                            <condition attribute='statecode' operator='eq' value='0' />
                          </filter>
                        </link-entity>
                      </entity>
                    </fetch>";
        }

        [DataContract]
        public class CyclePlanBatchProcess
        {
            [DataMember]
            public string GroupId { get; set; }
            [DataMember]
            public EntityReference CyclePlan { get; set; }
            [DataMember]
            public List<EntityReference> Records { get; set; }
            [DataMember]
            public bool BreakDownFurther { get; set; }
        }

    }
}
