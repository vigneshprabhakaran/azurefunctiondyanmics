﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SegmentCallPlan
{
    public static class CallPlanDistributionHelper
    {
        private static TraceWriter _Logger { get; set; }

        private static int BatchCount = 1;

        public enum BatchType
        {
            CustomerCallPlan,
            SalesCallPlan
        }

        public static void Distribute(Guid segmentCallPlanId, CrmServiceClient service, TraceWriter logger)
        {
            DateTime startTime = DateTime.Now;

            _Logger = logger;
            logger.Info("\tLoading Segment Data...");
            // Get Segment Call Plan Details
            DistributionData distribution = new DistributionData(segmentCallPlanId);

            distribution.LoadSegmentDetails(service);

            //Clear Old Data - FOR TESTING ONLY
            //logger.Info("\tClearing Old Data...");
            //distribution.CleanUp(service);

            //return;

            logger.Info("\tLoading Call Plan Data...");
            // Load Call Plan Data
            distribution.LoadCallPlanData(service);

            logger.Info("\tCreating Parent Records...");
            // Create Parent Records
            distribution.CreateParentRecords(service);

            logger.Info("\tCreating Child Records...");
            // Create Child Records
            distribution.CreateChildRecords(service);

            DateTime endTtime = DateTime.Now;
            TimeSpan ts = (endTtime - startTime);

            logger.Info(String.Format("Call Plan Generation Complete.  Total Time: ") + ts.ToString(@"hh\:mm\:ss"));

            Console.ReadKey();
        }


        #region Child Classes
        public class DistributionData
        {
            #region Properties
            public Guid SegmentCallPlanId { get; set; }
            public EntityReference SegmentCallPlan { get; set; }
            public EntityReference Product { get; set; }
            public EntityReference Segment { get; set; }
            public DateTime PeriodStartDate { get; set; }
            public DateTime PeriodEndDate { get; set; }
            public List<Guid> Positions { get; set; }
            public String Name { get; set; }
            public int Emails { get; set; }
            public int Calls { get; set; }
            public string Notes { get; set; }


            Dictionary<Guid, ParentSalesCallPlan> ParentCallPlans { get; set; }
            #endregion

            #region Constructor
            public DistributionData(Guid segmentCallPlanId)
            {
                this.SegmentCallPlanId = segmentCallPlanId;
                this.SegmentCallPlan = new EntityReference("indskr_cycleplan", segmentCallPlanId);
                this.ParentCallPlans = new Dictionary<Guid, ParentSalesCallPlan>();
                this.Positions = new List<Guid>();
            }
            #endregion

            #region Public Methods
            public void LoadSegmentDetails(CrmServiceClient service)
            {
                Entity segmentPlan = service.Retrieve("indskr_cycleplan", this.SegmentCallPlanId,
                    new ColumnSet("indskr_productid", "indskr_segment_v2", "indskr_cycleplanid", "indskr_name",
                    "indskr_startdate", "indskr_enddate", "indskr_hocalls", "indskr_hoemails", "indskr_cycleplannotes"));

                // Populate Data
                this.Product = (EntityReference)segmentPlan["indskr_productid"];
                this.Segment = (EntityReference)segmentPlan["indskr_segment_v2"];
                this.PeriodStartDate = (DateTime)segmentPlan["indskr_startdate"];
                this.PeriodEndDate = (DateTime)segmentPlan["indskr_enddate"];
                this.Name = segmentPlan["indskr_name"].ToString();
                this.Calls = (int)segmentPlan["indskr_hocalls"];
                this.Emails = (int)segmentPlan["indskr_hoemails"];
                //this.Notes = segmentPlan["indskr_cycleplannotes"].ToString();

                // Load Positions
                LoadProductPositions(service);
            }

            public void LoadCallPlanData(CrmServiceClient service)
            {
                // Load Data if not already loaded
                if (this.Product == null)
                {
                    LoadSegmentDetails(service);
                }

                // Define Condition Values
                var QEposition_statecode = 0;
                var QEposition_indskr_customerposition_statecode = 0;
                var QEposition_indskr_customerposition_contact_indskr_productrating_indskr_product = this.Product.Id.ToString();
                var QEposition_indskr_customerposition_contact_indskr_productrating_indskr_segmentation = this.Segment.Id.ToString();

                // Instantiate QueryExpression QEposition
                QueryExpression query = new QueryExpression("position");
                query.Distinct = true;
                query.PageInfo = new PagingInfo();
                query.PageInfo.Count = 5000;
                query.PageInfo.PageNumber = 1;
                query.PageInfo.PagingCookie = null;

                // Add columns to QEposition.ColumnSet
                query.ColumnSet.AddColumns("positionid");

                // Define filter QEposition.Criteria
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, QEposition_statecode);
                query.Criteria.AddCondition(new ConditionExpression("positionid", ConditionOperator.In, this.Positions.ToArray()));
                query.AddOrder("positionid", OrderType.Ascending);

                // Add link-entity QEposition_indskr_customerposition
                var QEposition_indskr_customerposition = query.AddLink("indskr_customerposition", "positionid", "indskr_positionid");

                // Define filter QEposition_indskr_customerposition.LinkCriteria
                QEposition_indskr_customerposition.LinkCriteria.AddCondition("statecode", ConditionOperator.Equal, QEposition_indskr_customerposition_statecode);

                // Add link-entity QEposition_indskr_customerposition_contact
                var QEposition_indskr_customerposition_contact = QEposition_indskr_customerposition.AddLink("contact", "indskr_customerid", "contactid");
                QEposition_indskr_customerposition_contact.EntityAlias = "Contact";
                QEposition_indskr_customerposition_contact.Orders.Add(new OrderExpression("contactid", OrderType.Ascending));

                // Add columns to QEposition_indskr_customerposition_contact.Columns
                QEposition_indskr_customerposition_contact.Columns.AddColumns("contactid", "fullname");

                // Add link-entity QEposition_indskr_customerposition_contact_indskr_productrating
                var QEposition_indskr_customerposition_contact_indskr_productrating = QEposition_indskr_customerposition_contact.AddLink("indskr_productrating", "contactid", "indskr_contact");

                // Define filter QEposition_indskr_customerposition_contact_indskr_productrating.LinkCriteria
                QEposition_indskr_customerposition_contact_indskr_productrating.LinkCriteria.AddCondition("indskr_product", ConditionOperator.Equal, QEposition_indskr_customerposition_contact_indskr_productrating_indskr_product);
                QEposition_indskr_customerposition_contact_indskr_productrating.LinkCriteria.AddCondition("indskr_segmentation", ConditionOperator.Equal, QEposition_indskr_customerposition_contact_indskr_productrating_indskr_segmentation);

                while (true)
                {
                    EntityCollection ec = service.RetrieveMultiple(query);
                    foreach (Entity results in ec.Entities)
                    {
                        #region do Work
                        // Get Attributes
                        Guid positionId = (Guid)results["positionid"];
                        Guid contactId = (Guid)((AliasedValue)results["Contact.contactid"]).Value;

                        // Set References
                        EntityReference position = new EntityReference("position", positionId);
                        EntityReference contact = new EntityReference("contact", contactId);

                        if (!this.ParentCallPlans.ContainsKey(contactId))
                        {
                            // Parent Call Plan
                            ParentSalesCallPlan newParentPlan = new ParentSalesCallPlan();
                            newParentPlan.Contact = contact;
                            newParentPlan.Id = Guid.NewGuid();

                            // Add to Dictionary
                            this.ParentCallPlans.Add(contactId, newParentPlan);
                        }

                        // Child Call Plan
                        SalesCallPlan salesCallPlan = new SalesCallPlan();
                        salesCallPlan.Contact = contact;
                        salesCallPlan.Position = position;

                        // Add to Parent Plan
                        ParentSalesCallPlan parentPlan = this.ParentCallPlans[contactId];
                        parentPlan.SalesCallPlans.Add(salesCallPlan);
                        #endregion

                    }

                    if (ec.MoreRecords)
                    {
                        query.PageInfo.PageNumber++;  // Paging Cookie does not work with Linkentity
                    }
                    else
                    {
                        break;  // Exit loop
                    }
                }
            }

            public void CreateParentRecords(CrmServiceClient service)
            {
                List<Task> batchRecordCreation = new List<Task>();
                // Loop Through and Process
                int batchSize = 500;
                EntityCollection batch = new EntityCollection();
                foreach (ParentSalesCallPlan parent in this.ParentCallPlans.Values)
                {
                    // Create Entity
                    Entity customerCallPlan = new Entity("indskr_customercallplan", parent.Id);
                    customerCallPlan["indskr_name"] = this.Name + " : " + parent.Contact.Name;
                    customerCallPlan["indskr_customerid"] = parent.Contact;
                    customerCallPlan["indskr_cycleplanid"] = this.SegmentCallPlan;
                    customerCallPlan["indskr_productid"] = this.Product;
                    customerCallPlan["indskr_segment_v2"] = this.Segment;
                    customerCallPlan["indskr_startdate"] = this.PeriodStartDate;
                    customerCallPlan["indskr_enddate"] = this.PeriodEndDate;
                    customerCallPlan["indskr_hocalls"] = this.Calls;
                    customerCallPlan["indskr_hoemails"] = this.Emails;
                    customerCallPlan["indskr_cycleplannotes"] = this.Notes;

                    customerCallPlan["statecode"] = new OptionSetValue(0);
                    customerCallPlan["statuscode"] = new OptionSetValue(1);

                    // Add to Batch
                    batch.Entities.Add(customerCallPlan);

                    if (batch.Entities.Count == batchSize)
                    {
                        var createRecords = Task.Run(() =>
                        {
                            var count = BatchCount;
                            var selectedBatch = batch.Entities;
                            CrmServiceClient serviceClient = DynamicsServiceClient.Instance.GetCrmServiceClientMultiUser(_Logger);
                            bool createResult = BulkCreate(serviceClient, selectedBatch); // Create
                            _Logger.Info($"\tCompleted Parent Records Batch {count}");
                        });
                        batchRecordCreation.Add(createRecords);
                        BatchCount++;
                        batch = new EntityCollection(); // Reset Batch
                    }
                }

                if (batch.Entities.Count > 0)
                {
                    // Partial Batch Remaining
                    BulkCreate(service, batch.Entities);
                }
                Task.WaitAll(batchRecordCreation.ToArray());
                BatchCount = 0;
            }

            public void CreateChildRecords(CrmServiceClient service)
            {
                List<Task> batchRecordCreation = new List<Task>();
                // Loop Through and Process
                int batchSize = 500;
                EntityCollection batch = new EntityCollection();
                foreach (ParentSalesCallPlan parent in this.ParentCallPlans.Values)
                {
                    foreach (SalesCallPlan child in parent.SalesCallPlans)
                    {
                        // Create Entity
                        Entity customerCallPlan = new Entity("indskr_customercallplan");
                        customerCallPlan["indskr_name"] = this.Name + " : " + parent.Contact.Name;
                        customerCallPlan["indskr_customerid"] = parent.Contact;
                        customerCallPlan["indskr_cycleplanid"] = this.SegmentCallPlan;
                        customerCallPlan["indskr_productid"] = this.Product;
                        customerCallPlan["indskr_segment_v2"] = this.Segment;
                        customerCallPlan["indskr_positionid"] = child.Position; // Position
                        customerCallPlan["indskr_startdate"] = this.PeriodStartDate;
                        customerCallPlan["indskr_enddate"] = this.PeriodEndDate;
                        customerCallPlan["indskr_hocalls"] = this.Calls;
                        customerCallPlan["indskr_hoemails"] = this.Emails;
                        customerCallPlan["indskr_cycleplannotes"] = this.Notes;
                        customerCallPlan["indskr_parentcustomercallplanid"] = new EntityReference("indskr_customercallplan", parent.Id);  // Set Parent
                        customerCallPlan["indskr_isrepcallplan"] = true;

                        customerCallPlan["statecode"] = new OptionSetValue(0);
                        customerCallPlan["statuscode"] = new OptionSetValue(1);

                        // Add to Batch
                        batch.Entities.Add(customerCallPlan);

                        if (batch.Entities.Count == batchSize)
                        {
                            var createRecords = Task.Run(() =>
                            {
                                var count = BatchCount;
                                var selectedBatch = batch.Entities;
                                CrmServiceClient serviceClient = DynamicsServiceClient.Instance.GetCrmServiceClientMultiUser(_Logger);
                                BulkCreate(serviceClient, selectedBatch); // Create
                                _Logger.Info($"\tCompleted Batch {count}");
                            });
                            BatchCount++;
                            batch = new EntityCollection(); // Reset Batch
                        }
                    }
                }

                if (batch.Entities.Count > 0)
                {
                    // Partial Batch Remaining
                    BulkCreate(service, batch.Entities);
                }
                Task.WaitAll(batchRecordCreation.ToArray());
                BatchCount = 0;
            }

            public void CleanUp(CrmServiceClient service)
            {
                QueryExpression query = new QueryExpression(EntityAttrs.CustomerCallPlan.EntityName);
                query.Criteria.AddCondition(EntityAttrs.CustomerCallPlan.CallPlan, ConditionOperator.Equal, this.SegmentCallPlanId);
                query.PageInfo = new PagingInfo();
                query.PageInfo.Count = 300; // ExecuteMultiple Size
                query.PageInfo.PageNumber = 1;
                query.PageInfo.PagingCookie = null;

                while (true)
                {
                    EntityCollection ec = service.RetrieveMultiple(query);
                    BulkDelete(service, ec.Entities);

                    if (ec.MoreRecords)
                    {
                        query.PageInfo.PageNumber++;
                        query.PageInfo.PagingCookie = ec.PagingCookie;
                    }
                    else
                    {
                        break;  // Exit loop
                    }
                }
            }
            #endregion

            #region Private Methods    
            private void LoadProductPositions(CrmServiceClient service)
            {
                QueryExpression query = new QueryExpression("indskr_productassignment");
                query.ColumnSet = new ColumnSet("indskr_positionid");
                query.Criteria.AddCondition(new ConditionExpression("indskr_productid", ConditionOperator.Equal, this.Product.Id));

                EntityCollection ec = service.RetrieveMultiple(query);

                foreach (Entity prodAssignment in ec.Entities)
                {
                    EntityReference refPosition = (EntityReference)prodAssignment["indskr_positionid"];
                    if (!this.Positions.Contains(refPosition.Id))
                    {
                        this.Positions.Add(refPosition.Id);
                    }
                }

            }

            private static bool BulkCreate(CrmServiceClient service, DataCollection<Entity> entities)
            {
                // Create an ExecuteMultipleRequest object.
                var multipleRequest = new ExecuteMultipleRequest()
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };

                // Add a CreateRequest for each entity to the request collection.
                foreach (var entity in entities)
                {
                    CreateRequest createRequest = new CreateRequest { Target = entity };
                    multipleRequest.Requests.Add(createRequest);
                }
                try
                {
                    ExecuteMultipleResponse resp = (ExecuteMultipleResponse)service.Execute(multipleRequest);

                    if (resp.IsFaulted)
                    {
                        return false;
                    }

                    return true;
                }
                catch (TimeoutException ex)
                {
                    Thread.Sleep(10000);
                    ExecuteMultipleResponse resp = (ExecuteMultipleResponse)service.Execute(multipleRequest);

                    if (resp.IsFaulted)
                    {
                        return false;
                    }

                    return true;
                }
                // Execute all the requests in the request collection using a single web method call.
            }
            private static void BulkDelete(CrmServiceClient service, DataCollection<Entity> entities)
            {
                // Create an ExecuteMultipleRequest object.
                var multipleRequest = new ExecuteMultipleRequest()
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };

                // Add a DeleteRequest for each entity to the request collection.
                foreach (var entity in entities)
                {
                    DeleteRequest deleteRequest = new DeleteRequest { Target = entity.ToEntityReference() };
                    multipleRequest.Requests.Add(deleteRequest);
                }

                // Execute all the requests in the request collection using a single web method call.
                ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequest);
            }
            #endregion
        }

        public class ParentSalesCallPlan
        {
            public ParentSalesCallPlan()
            {
                this.SalesCallPlans = new List<SalesCallPlan>();
            }

            public Guid Id { get; set; }
            public EntityReference Contact { get; set; }
            public EntityReference CustomerSegment { get; set; }
            public List<SalesCallPlan> SalesCallPlans { get; set; }
        }

        public class SalesCallPlan
        {
            public Guid Id { get; set; }
            public EntityReference Contact { get; set; }
            public EntityReference CustomerSegment { get; set; }
            public EntityReference Position { get; set; }
        }
        #endregion
    }
}

