﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Description;
using System.Text;
using System.Xml;

namespace SegmentCallPlan
{
    public static class Common
    {
        private static CrmServiceClient _svc { get; set; }

        private static int _instanceCount = 2;

        private static readonly object padlock = new object();

        public static CrmServiceClient serviceClient {
            get
            {
                if (true)
                {

                }
                string serviceAccount = "IndegeneServiceAccount" + _instanceCount;
                CrmServiceClient svc = new CrmServiceClient((ConfigurationManager.ConnectionStrings[serviceAccount].ConnectionString));
                return svc;
            }
          }

        #region Task Functions

        public static CrmServiceClient GetCrmServiceClient(Microsoft.Azure.WebJobs.Host.TraceWriter _Logger)
        {
            if (_svc != null && _svc.IsReady)
            {
                return _svc;
            }
            else
            {
                lock (padlock)
                {
                    if (_svc == null)
                    {
                        string serviceAccount = "IndegeneServiceAccount" + _instanceCount;
                        _Logger.Info($"Connecting to {serviceAccount}");
                        _svc = new CrmServiceClient(ConfigurationManager.ConnectionStrings[serviceAccount].ConnectionString);
                        _instanceCount = (_instanceCount < 6) ? _instanceCount++ : 1;
                       
                    }
                }
                return _svc;
            }

        }

        public static void StartFirstBatch(string groupId, CrmServiceClient service)
        {
            QueryExpression nextBatchQueryExpression = new QueryExpression();
            nextBatchQueryExpression.EntityName = "indskr_batchprocess";
            nextBatchQueryExpression.ColumnSet = new ColumnSet(false);
            nextBatchQueryExpression.Criteria.AddCondition("indskr_sequence", ConditionOperator.Equal, 1);
            nextBatchQueryExpression.Criteria.AddCondition("indskr_groupid", ConditionOperator.Equal, groupId);

            var batchResults = service.RetrieveMultiple(nextBatchQueryExpression);
            if (batchResults.Entities.Any())
            {
                Entity nextBatch = batchResults.Entities.FirstOrDefault();
                nextBatch["indskr_inprogress"] = true;
                service.Update(nextBatch);
            }
        }
        #endregion

        #region Generic Functions 

        public static T GetAttributeValue<T>(Entity targetEntity, string attributeName)
        {
            try
            {
                object result = default(T);
                if (targetEntity != null && targetEntity.Attributes.Contains(attributeName))
                {
                    result = targetEntity.Attributes[attributeName];
                }

                return (T)result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static void SetAttributeValue(Entity entity, string attributeName, object value)
        {
            try
            {
                if (entity.Attributes.Contains(attributeName))
                {
                    entity.Attributes[attributeName] = value;
                }
                else
                {
                    entity.Attributes.Add(attributeName, value);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static List<List<T>> Split<T>(List<T> collection, int size)
        {
            var chunks = new List<List<T>>();
            var chunkCount = collection.Count() / size;

            if (collection.Count % size > 0)
                chunkCount++;

            for (var i = 0; i < chunkCount; i++)
                chunks.Add(collection.Skip(i * size).Take(size).ToList());

            return chunks;
        }

        public static string SerializeObjectToJSON(object obj)
        {
            DataContractJsonSerializer json = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            XmlDictionaryWriter writer = JsonReaderWriterFactory.CreateJsonWriter(ms);
            json.WriteObject(ms, obj);
            writer.Flush();
            return Encoding.UTF8.GetString(ms.GetBuffer()).Trim().TrimEnd('\0');
        }

        public static T DeserializeJSONToObject<T>(string json)
        {
            T deserializedObject;
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            deserializedObject = (T)ser.ReadObject(ms);
            ms.Close();
            return deserializedObject;
        }

        public static SetStateRequest SetStateAndStatusReason(EntityReference entityReference, int stateCode, int statusReason)
        {
            SetStateRequest state = new SetStateRequest();
            state.EntityMoniker = entityReference;
            state.State = new OptionSetValue(stateCode);
            state.Status = new OptionSetValue(statusReason);
            return state;
        }

        public static SetStateResponse SetState(EntityReference entityReference, int stateCode, int statusCode, IOrganizationService service)
        {
            SetStateRequest state = new SetStateRequest();

            state.EntityMoniker = entityReference;

            state.State = new OptionSetValue(stateCode);
            state.Status = new OptionSetValue(statusCode);

            return (SetStateResponse)service.Execute(state);
        }

        public static EntityCollection GetRandomEntityRecord(IOrganizationService service, string entityLogicalName, List<string> relatedEntityAttribute, List<string> relatedentityValues, ColumnSet columnset)
        {
            EntityCollection entityRecord = new EntityCollection();
            try
            {
                QueryExpression query = new QueryExpression(entityLogicalName);
                query.ColumnSet = columnset;
                FilterExpression childFilter = query.Criteria.AddFilter(LogicalOperator.And);
                childFilter.AddCondition(relatedEntityAttribute[0], ConditionOperator.Equal, relatedentityValues[0]);
                childFilter.AddCondition(relatedEntityAttribute[1], ConditionOperator.Equal, relatedentityValues[1]);

                entityRecord = service.RetrieveMultiple(query);
                return entityRecord;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion



    }
}
