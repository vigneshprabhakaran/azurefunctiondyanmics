﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegmentCallPlan
{
    public class EntityAttrs
    {

        internal struct AccountContactAffiliation
        {
            public const string EntityName = "indskr_accountcontactaffiliation";
            public const string Contact = "indskr_contactid";
            public const string ID = "indskr_accountcontactaffiliationid";
            public const string Account = "indskr_accountid";
            public const string Comments = "indskr_comments";
            public const string StartDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string StatusCode = "statuscode";
            public const string DirectRelationshipFlag = "indskr_directrelationshipflag";
            public const string Role = "indskr_contactrole";

        }

        internal struct CustomerCallPlan
        {
            public const string Name = "indskr_name";
            public const string EntityName = "indskr_customercallplan";
            public const string ID = "indskr_customercallplanid";
            public const string UserTimeZone = "usersettings5.timezonecode";
            public const string UserId = "usersettings5.systemuserid";
            public const string ParentPosition = "ParentPosition.name";
            public const string StatusCode = "statuscode";
            public const string Customer = "indskr_customerid";
            public const string Product = "indskr_productid";
            public const string CustomerSegment = "indskr_customersegment";
            public const string State = "indskr_state";
            public const string CallPlan = "indskr_cycleplanid";
            public const string IsRepCallPlan = "indskr_isrepcallplan";
            public const string ActualCalls = "indskr_actualcalls";
            public const string ActualEmails = "indskr_actualemails";
            public const string PeriodStartDate = "indskr_startdate";
            public const string PeriodEndDate = "indskr_enddate";
            public const string Position = "indskr_positionid";
            public const string Recalculate = "indskr_recalculate";

            //internal struct Relationships
            //{
            //    public const string 
            //}
        }
    }
}
