﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegmentCallPlan
{
    public class SegmentCallPlanRequestBody
    {
        public Guid SegmentCallPlanId { get; set; }
    }

    public class BatchProcessRequestBody
    {
        public Guid BatchProcessId { get; set; }
    }
}
