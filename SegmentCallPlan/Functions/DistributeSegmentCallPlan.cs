using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using static SegmentCallPlan.Common;
using static SegmentCallPlan.CustomerCallPlanConstants;

namespace SegmentCallPlan
{
    public static class DistributeSegmentCallPlan
    {
        #region Private Properties
        private static CrmServiceClient _Service { get; set; }
        private static TraceWriter _Logger { get; set; }
        #endregion


        [FunctionName("DistributeSegmentCallPlan")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "distribute")]
            HttpRequestMessage req,
            [Queue("segment-call-plan-ids")] IAsyncCollector<string> segmentCallPlanId,
            TraceWriter log)
        {
            try
            {
                log.Info("Requested for Distribution of Segment call Plan ");

                _Logger = log;

                _Service = GetCrmServiceClient(_Logger);

                SegmentCallPlanRequestBody requestBody = await req.Content.ReadAsAsync<SegmentCallPlanRequestBody>();

                await segmentCallPlanId.AddAsync(requestBody.SegmentCallPlanId.ToString());

                log.Info("Call Plan Distributed");

                return req.CreateResponse(HttpStatusCode.OK,"The Segment Call Plan is Pushed into the Azure Queue");
            }
            catch (ArgumentNullException ex)
            {
                log.Error($"Error - Message: {ex.Message} ");
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {

                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                log.Error($"Error - Message: {ex.Message} ");
                log.Info($"Line Number {line}");
                log.Error($"Error - StackTrace: {ex.StackTrace} ");
                return req.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }
    }
}
